const rp = require('request-promise');
const cheerio = require('cheerio');
const fs = require('fs');
const Nightmare = require('nightmare');
const nightmare = Nightmare({show: true});
const writeStream = fs.createWriteStream('post.csv');

// подготавливает выходной файл
writeStream.write(`Time, Team Left, Team Right, Event, Maps \n`);

const URL = 'https://www.hltv.org/matches/';

// создает запрос через Nightmare
function getPage() {
    nightmare
        .goto(URL)
        .wait('body')
        .evaluate(() => document.querySelector('body').innerHTML)
        .end()
        .then(response => {
            console.log(getData(response));
        }).catch(err => {
        console.log(err);
    })
}

// Parsing
function getUpcomingMatches() {
    getPage();
    let data;
    let patternRegExp = new RegExp(/\s\s+/g);
    let getData = html => {
        data = [];
        const $ = cheerio.load(html);
        $('.upcoming-matches').each((row, raw_element) => {
            $(raw_element).find('.match').each((i, elem) => {
                let leftTeam = $(elem)
                    .find('table > tbody > tr > td:nth-child(2) > div.line-align')
                    .text()
                    .replace(patternRegExp, '');
                let rightTeam = $(elem)
                    .find('table > tbody > tr > td:nth-child(4) > div.line-align')
                    .text()
                    .replace(patternRegExp, '');
                let time = $(elem)
                    .find('div > a.a-reset > table > tbody > tr > td.time')
                    .text()
                    .replace(patternRegExp, '');
                let event = $(elem)
                    .find('div > a.a-reset > table > tbody > tr > td.event > span')
                    .text()
                    .replace(patternRegExp, '');
                let maps = $(elem)
                    .find('div > a.a-reset > table > tbody > tr > td.star-cell > div > .map-text')
                    .text()
                    .replace(patternRegExp, '');
                let link = $(elem)
                    .find('a')
                    .attr('href');

                data.push({
                    'leftTeamName': leftTeam,
                    'rightTeamName': rightTeam,
                    'matchStartTime': time,
                    'matchEvent': event,
                    'matchFormat': maps,
                    'matchLink': 'https://www.hltv.org' + link
                });

                // пишем записи в CSV by fs
                writeStream.write(`${leftTeam}, ${rightTeam}, ${time}, ${event}, ${maps}, https://www.hltv.org${link}  \n`);
            });
        });
    };

    return data;
}


// возвращает массив с ссылками на LIVE  матчи
function getLiveMatchesLinks() {
    getPage();
    let links;
    let getData = html => {
        links = [];
        const $ = cheerio.load(html);
        $('.live-matches').each((row, raw_element) => {
            $(raw_element).find('.live-match:not(:last-child)').each((i, elem) => {
                links[i] = $(elem)
                    .find('a.a-reset')
                    .attr('href');
            });
            console.log(links.length)
        });

        return links;
    };
}

getLiveMatchesLinks();

// парсит детальную страницу LIVE матча
function getLiveMatchesScore() {

    // здесь ожидался массив возвращаемый функцией "function getLiveMatchesLinks()"
    // подставлены ссылки на LIVE матчи
    let URLS = ['https://www.hltv.org/matches/2340861/copenhagen-flames-vs-g2-esl-one-road-to-rio-europe',
                'https://www.hltv.org/matches/2340862/north-vs-godsent-esl-one-road-to-rio-europe',
                'https://www.hltv.org/matches/2340863/c0ntact-vs-mousesports-esl-one-road-to-rio-europe'];

    for (let i = 0; i < URLS.length; i++) {
        let currentURL = URLS[i];
        rp(currentURL, (error, response, html) => {
            if (!error) {
                const $ = cheerio.load(html);

                $('.team').each((row, raw_element) => {
                    $(raw_element).find('.team1-gradient').each((i, el) => {
                        const team = $(el)
                            .find('.teamName')
                            .text();
                        console.log(team); // команда слева
                    })
                })
            }
        })
    }
}


// .....
